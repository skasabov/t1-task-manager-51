package ru.t1.skasabov.tm.taskmanager.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.model.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.model.ISessionRepository;
import ru.t1.skasabov.tm.api.repository.model.ITaskRepository;
import ru.t1.skasabov.tm.api.repository.model.IUserRepository;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.model.ProjectRepository;
import ru.t1.skasabov.tm.repository.model.SessionRepository;
import ru.t1.skasabov.tm.repository.model.TaskRepository;
import ru.t1.skasabov.tm.repository.model.UserRepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;
import ru.t1.skasabov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;

public class UserRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User cat;

    @NotNull
    private User mouse;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private IProjectRepository projectRepository;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private EntityManager entityManager;

    @Before
    @SneakyThrows
    public void initRepository() {
        entityManager = connectionService.getEntityManager();
        userRepository = new UserRepository(entityManager);
        taskRepository = new TaskRepository(entityManager);
        projectRepository = new ProjectRepository(entityManager);
        sessionRepository = new SessionRepository(entityManager);
        entityManager.getTransaction().begin();
        @NotNull final List<Task> catTaskList = new ArrayList<>();
        @NotNull final List<Project> catProjectList = new ArrayList<>();
        @NotNull final List<Session> catSessionList = new ArrayList<>();
        @NotNull final List<Task> mouseTaskList = new ArrayList<>();
        @NotNull final List<Project> mouseProjectList = new ArrayList<>();
        @NotNull final List<Session> mouseSessionList = new ArrayList<>();

        cat = new User();
        cat.setLogin("cat");
        @Nullable final String passwordHashCat = HashUtil.salt(propertyService, "cat");
        Assert.assertNotNull(passwordHashCat);
        cat.setPasswordHash(passwordHashCat);
        cat.setEmail("cat@cat");
        @NotNull final Project catProject = new Project();
        catProject.setName("cat");
        catProject.setUser(cat);
        @NotNull final Task catTask = new Task();
        catTask.setName("cat");
        catTask.setUser(cat);
        @NotNull final Session catSession = new Session();
        catSession.setUser(cat);
        catProjectList.add(catProject);
        catSessionList.add(catSession);
        catTaskList.add(catTask);
        cat.setProjects(catProjectList);
        cat.setTasks(catTaskList);
        cat.setSessions(catSessionList);

        mouse = new User();
        mouse.setLogin("mouse");
        @Nullable final String passwordHashMouse = HashUtil.salt(propertyService, "mouse");
        Assert.assertNotNull(passwordHashMouse);
        mouse.setPasswordHash(passwordHashMouse);
        mouse.setEmail("mouse@mouse");
        @NotNull final Project mouseProject = new Project();
        mouseProject.setName("mouse");
        mouseProject.setUser(mouse);
        @NotNull final Task mouseTask = new Task();
        mouseTask.setName("mouse");
        mouseTask.setUser(mouse);
        @NotNull final Session mouseSession = new Session();
        mouseSession.setUser(mouse);
        mouseProjectList.add(mouseProject);
        mouseSessionList.add(mouseSession);
        mouseTaskList.add(mouseTask);
        mouse.setProjects(mouseProjectList);
        mouse.setTasks(mouseTaskList);
        mouse.setSessions(mouseSessionList);

        userRepository.add(cat);
        userRepository.add(mouse);
    }

    @Test
    public void testUpdate() {
        mouse.setLastName("mouse");
        mouse.setFirstName("mouse");
        mouse.setMiddleName("mouse");
        userRepository.update(mouse);
        @Nullable final User actualUser = userRepository.findByLogin("mouse");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals("mouse", actualUser.getLastName());
        Assert.assertEquals("mouse", actualUser.getFirstName());
        Assert.assertEquals("mouse", actualUser.getMiddleName());
    }

    @Test
    public void testAdd() {
        final long expectedUsers = userRepository.getSize() + 1;
        final long expectedTasks = taskRepository.getSize() + 2;
        final long expectedProjects = projectRepository.getSize() + 1;
        final long expectedSessions = sessionRepository.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        @Nullable final String passwordHashDog = HashUtil.salt(propertyService, "dog");
        Assert.assertNotNull(passwordHashDog);
        user.setPasswordHash(passwordHashDog);
        @NotNull final Project userProject = new Project();
        userProject.setName("dog");
        userProject.setUser(user);
        @NotNull final Task userTask = new Task();
        userTask.setName("dog");
        userTask.setUser(user);
        @NotNull final Session userSession = new Session();
        userSession.setUser(user);
        @NotNull final Task task = new Task();
        task.setName("task");
        task.setProject(userProject);
        task.setUser(user);
        @NotNull final List<Task> projectTasks = new ArrayList<>();
        projectTasks.add(task);
        userProject.setTasks(projectTasks);
        @NotNull final List<Task> userTaskList = new ArrayList<>();
        @NotNull final List<Project> userProjectList = new ArrayList<>();
        @NotNull final List<Session> userSessionList = new ArrayList<>();
        userProjectList.add(userProject);
        userSessionList.add(userSession);
        userTaskList.add(userTask);
        user.setProjects(userProjectList);
        user.setTasks(userTaskList);
        user.setSessions(userSessionList);
        userRepository.add(user);
        Assert.assertEquals(expectedUsers, userRepository.getSize());
        Assert.assertEquals(expectedTasks, taskRepository.getSize());
        Assert.assertEquals(expectedProjects, projectRepository.getSize());
        Assert.assertEquals(expectedSessions, sessionRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = userRepository.getSize() + 4;
        final long expectedNumberOfTasks = taskRepository.getSize() + 8;
        final long expectedNumberOfProjects = projectRepository.getSize() + 4;
        final long expectedNumberOfSessions = sessionRepository.getSize() + 4;
        @NotNull final List<User> actualUsers = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            @Nullable final String passwordHashUser = HashUtil.salt(propertyService, "user " + i);
            Assert.assertNotNull(passwordHashUser);
            user.setPasswordHash(passwordHashUser);
            @NotNull final Project userProject = new Project();
            userProject.setName("user " + i);
            userProject.setUser(user);
            @NotNull final Task userTask = new Task();
            userTask.setName("user " + i);
            userTask.setUser(user);
            @NotNull final Session userSession = new Session();
            userSession.setUser(user);
            @NotNull final Task task = new Task();
            task.setName("task");
            task.setProject(userProject);
            task.setUser(user);
            @NotNull final List<Task> projectTasks = new ArrayList<>();
            projectTasks.add(task);
            userProject.setTasks(projectTasks);
            @NotNull final List<Task> userTaskList = new ArrayList<>();
            @NotNull final List<Project> userProjectList = new ArrayList<>();
            @NotNull final List<Session> userSessionList = new ArrayList<>();
            userProjectList.add(userProject);
            userSessionList.add(userSession);
            userTaskList.add(userTask);
            user.setProjects(userProjectList);
            user.setTasks(userTaskList);
            user.setSessions(userSessionList);
            actualUsers.add(user);
        }
        userRepository.addAll(actualUsers);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<User> actualUsers = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setLogin("user " + i);
            @Nullable final String passwordHashUser = HashUtil.salt(propertyService, "user " + i);
            Assert.assertNotNull(passwordHashUser);
            user.setPasswordHash(passwordHashUser);
            @NotNull final Project userProject = new Project();
            userProject.setName("user " + i);
            userProject.setUser(user);
            @NotNull final Task userTask = new Task();
            userTask.setName("user " + i);
            userTask.setUser(user);
            @NotNull final Session userSession = new Session();
            userSession.setUser(user);
            @NotNull final Task task = new Task();
            task.setName("task");
            task.setProject(userProject);
            task.setUser(user);
            @NotNull final List<Task> projectTasks = new ArrayList<>();
            projectTasks.add(task);
            userProject.setTasks(projectTasks);
            @NotNull final List<Task> userTaskList = new ArrayList<>();
            @NotNull final List<Project> userProjectList = new ArrayList<>();
            @NotNull final List<Session> userSessionList = new ArrayList<>();
            userProjectList.add(userProject);
            userSessionList.add(userSession);
            userTaskList.add(userTask);
            user.setProjects(userProjectList);
            user.setTasks(userTaskList);
            user.setSessions(userSessionList);
            actualUsers.add(user);
        }
        userRepository.set(actualUsers);
        Assert.assertEquals(NUMBER_OF_ENTRIES, userRepository.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, projectRepository.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES * 2, taskRepository.getSize());
        Assert.assertEquals(NUMBER_OF_ENTRIES, sessionRepository.getSize());
    }

    @Test
    public void testClearAll() {
        userRepository.removeAll();
        Assert.assertEquals(0, userRepository.getSize());
        Assert.assertEquals(0, projectRepository.getSize());
        Assert.assertEquals(0, taskRepository.getSize());
        Assert.assertEquals(0, sessionRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = userRepository.getSize() - 2;
        final long expectedNumberOfTasks = taskRepository.getSize() - 2;
        final long expectedNumberOfProjects = projectRepository.getSize() - 2;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 2;
        @NotNull final List<User> userList = new ArrayList<>();
        userList.add(cat);
        userList.add(mouse);
        userRepository.removeAll(userList);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> userList = userRepository.findAll();
        Assert.assertEquals(userList.size(), userRepository.getSize());
    }

    @Test
    public void testFindById() {
        @Nullable final User actualUser = userRepository.findOneById(cat.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIdUserNotFound() {
        Assert.assertNull(userRepository.findOneById("some_id"));
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User actualUser = userRepository.findByLogin("cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByLoginUserNotFound() {
        Assert.assertNull(userRepository.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User actualUser = userRepository.findByEmail("cat@cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByEmailUserNotFound() {
        Assert.assertNull(userRepository.findByEmail("dog@dog"));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final User user = userRepository.findAll().get(0);
        @Nullable final User actualUser = userRepository.findOneByIndex(0);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getLogin(), actualUser.getLogin());
        Assert.assertEquals(user.getEmail(), actualUser.getEmail());
        Assert.assertEquals(user.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIndexUserNotFound() {
        Assert.assertNull(userRepository.findOneByIndex(2));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        @Nullable final String passwordHashDog = HashUtil.salt(propertyService, "dog");
        Assert.assertNotNull(passwordHashDog);
        user.setPasswordHash(passwordHashDog);
        user.setEmail("dog@dog");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = userRepository.findAll().get(0).getId();
        @NotNull final String invalidId = "some_id";
        Assert.assertFalse(userRepository.existsById(invalidId));
        Assert.assertTrue(userRepository.existsById(validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = userRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        final long expectedNumberOfProjects = projectRepository.getSize() - 1;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 1;
        userRepository.removeOne(mouse);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = userRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        final long expectedNumberOfProjects = projectRepository.getSize() - 1;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 1;
        userRepository.removeOneById(mouse.getId());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = userRepository.getSize() - 1;
        final long expectedNumberOfTasks = taskRepository.getSize() - 1;
        final long expectedNumberOfProjects = projectRepository.getSize() - 1;
        final long expectedNumberOfSessions = sessionRepository.getSize() - 1;
        userRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
        Assert.assertEquals(expectedNumberOfTasks, taskRepository.getSize());
        Assert.assertEquals(expectedNumberOfProjects, projectRepository.getSize());
        Assert.assertEquals(expectedNumberOfSessions, sessionRepository.getSize());
    }

    @Test
    public void testLoginExist() {
        Assert.assertTrue(userRepository.isLoginExist("cat"));
    }

    @Test
    public void testEmptyLoginExist() {
        Assert.assertFalse(userRepository.isLoginExist(""));
    }

    @Test
    public void testEmailExist() {
        Assert.assertTrue(userRepository.isEmailExist("cat@cat"));
    }

    @Test
    public void testEmptyEmailExist() {
        Assert.assertFalse(userRepository.isEmailExist(""));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

}
