package ru.t1.skasabov.tm.taskmanager.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.model.IProjectRepository;
import ru.t1.skasabov.tm.api.repository.model.ITaskRepository;
import ru.t1.skasabov.tm.api.repository.model.IUserRepository;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;
import ru.t1.skasabov.tm.model.Task;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.repository.model.ProjectRepository;
import ru.t1.skasabov.tm.repository.model.TaskRepository;
import ru.t1.skasabov.tm.repository.model.UserRepository;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;

import javax.persistence.EntityManager;
import java.util.*;
import java.util.stream.Collectors;

public class TaskRepositoryTest extends AbstractTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static User userOne;

    @NotNull
    private static User userTwo;

    @NotNull
    private static String USER_ID_ONE;

    @NotNull
    private static String USER_ID_TWO;

    @NotNull
    private Project projectOne;

    @NotNull
    private Project projectTwo;

    @NotNull
    private ITaskRepository taskRepository;

    @NotNull
    private EntityManager entityManager;

    @Before
    @SneakyThrows
    public void initRepository() {
        final long currentTime = System.currentTimeMillis();
        entityManager = connectionService.getEntityManager();
        @NotNull final IProjectRepository projectRepository = new ProjectRepository(entityManager);
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        taskRepository = new TaskRepository(entityManager);
        entityManager.getTransaction().begin();
        userOne = new User();
        userOne.setLogin("user_one");
        userOne.setPasswordHash("user_one");
        userTwo = new User();
        userTwo.setLogin("user_two");
        userTwo.setPasswordHash("user_two");
        userRepository.add(userOne);
        userRepository.add(userTwo);
        USER_ID_ONE = userOne.getId();
        USER_ID_TWO = userTwo.getId();
        projectOne = new Project();
        projectOne.setUser(userOne);
        projectOne.setName("project_one");
        projectTwo = new Project();
        projectTwo.setUser(userTwo);
        projectTwo.setName("project_two");
        projectRepository.add(projectOne);
        projectRepository.add(projectTwo);
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            task.setCreated(new Date(currentTime + i * 1000));
            if (i < 4) task.setStatus(Status.COMPLETED);
            else if (i < 7) task.setStatus(Status.IN_PROGRESS);
            else task.setStatus(Status.NOT_STARTED);
            if (i <= 5) {
                task.setUser(userOne);
                task.setProject(projectOne);
            } else {
                task.setUser(userTwo);
                task.setProject(projectTwo);
            }
            taskRepository.add(task);
        }
    }

    @Test
    public void testUpdate() {
        @NotNull final Task task = taskRepository.findAll(USER_ID_TWO).get(0);
        task.setName("Test Task One");
        task.setDescription("Test Description One");
        taskRepository.update(task);
        @Nullable final Task actualTask = taskRepository.findOneById(USER_ID_TWO, task.getId());
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(USER_ID_TWO, actualTask.getUser().getId());
        Assert.assertEquals("Test Task One", actualTask.getName());
        Assert.assertEquals("Test Description One", actualTask.getDescription());
    }

    @Test
    public void testAdd() {
        final long expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUser(userTwo);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testAddAll() {
        final long expectedNumberOfEntries = taskRepository.getSize() + 4;
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= 4; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test Task " + i);
            task.setUser(userOne);
            actualTasks.add(task);
        }
        taskRepository.addAll(actualTasks);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testSet() {
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test Task " + i);
            task.setUser(userTwo);
            actualTasks.add(task);
        }
        taskRepository.set(actualTasks);
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskRepository.getSize());
    }

    @Test
    public void testClearAll() {
        taskRepository.removeAll();
        Assert.assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testClearAllForUser() {
        final long expectedNumberOfEntries = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        taskRepository.removeAll(USER_ID_ONE);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testClear() {
        final long expectedNumberOfEntries = taskRepository.getSize() - NUMBER_OF_ENTRIES / 2;
        @NotNull final List<Task> taskList = taskRepository.findAll(USER_ID_TWO);
        taskRepository.removeAll(taskList);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> taskList = taskRepository.findAll();
        Assert.assertEquals(taskList.size(), taskRepository.getSize());
    }

    @Test
    public void testFindAllWithNameComparator() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByName();
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        actualTasks.add(1, actualTasks.get(NUMBER_OF_ENTRIES - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithCreatedComparator() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByCreated();
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithStatusComparator() {
        @NotNull final List<Status> statusList = taskRepository.findAllSortByStatus()
                .stream().map(Task::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            if (i < 4) actualStatuses.add(Status.COMPLETED);
            else if (i < 7) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindAllForUser() {
        @NotNull final List<Task> taskList = taskRepository.findAll(USER_ID_ONE);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2, taskList.size());
    }

    @Test
    public void testFindAllWithNameComparatorForUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByNameForUser(USER_ID_TWO);
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + (i + 5));
            actualTasks.add(task);
        }
        actualTasks.add(0, actualTasks.get(NUMBER_OF_ENTRIES / 2 - 1));
        actualTasks.remove(NUMBER_OF_ENTRIES / 2);
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithCreatedComparatorForUser() {
        @NotNull final List<Task> taskSortList = taskRepository.findAllSortByCreatedForUser(USER_ID_ONE);
        @NotNull final List<Task> actualTasks = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            @NotNull final Task task = new Task();
            task.setName("Test " + i);
            actualTasks.add(task);
        }
        Assert.assertEquals(actualTasks, taskSortList);
    }

    @Test
    public void testFindAllWithStatusComparatorForUser() {
        @NotNull final List<Status> statusList = taskRepository.findAllSortByStatusForUser(USER_ID_TWO)
                .stream().map(Task::getStatus).collect(Collectors.toList());
        @NotNull final List<Status> actualStatuses = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ENTRIES / 2; i++) {
            if (i < 2) actualStatuses.add(Status.IN_PROGRESS);
            else actualStatuses.add(Status.NOT_STARTED);
        }
        Assert.assertEquals(actualStatuses, statusList);
    }

    @Test
    public void testFindById() {
        @NotNull final Task task = taskRepository.findAll().get(0);
        @NotNull final String taskId = taskRepository.findAll().get(0).getId();
        @Nullable final Task actualTask = taskRepository.findOneById(taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), actualTask.getUser().getId());
    }

    @Test
    public void testFindByIdForUser() {
        @NotNull final Task task = taskRepository.findAll(USER_ID_ONE).get(0);
        @NotNull final String taskId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        @Nullable final Task actualTask = taskRepository.findOneById(USER_ID_ONE, taskId);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), actualTask.getUser().getId());
    }

    @Test
    public void testFindByIdTaskNotFound() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(id));
    }

    @Test
    public void testFindByIdTaskNotFoundForUser() {
        @NotNull final String id = UUID.randomUUID().toString();
        Assert.assertNull(taskRepository.findOneById(USER_ID_ONE, id));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final Task task = taskRepository.findAll().get(0);
        @Nullable final Task actualTask = taskRepository.findOneByIndex(0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), actualTask.getUser().getId());
    }

    @Test
    public void testFindByIndexTaskNotFound() {
        Assert.assertNull(taskRepository.findOneByIndex(NUMBER_OF_ENTRIES));
    }

    @Test
    public void testFindByIndexForUser() {
        @NotNull final Task task = taskRepository.findAll(USER_ID_TWO).get(0);
        @Nullable final Task actualTask = taskRepository.findOneByIndex(USER_ID_TWO, 0);
        Assert.assertNotNull(actualTask);
        Assert.assertEquals(task.getName(), actualTask.getName());
        Assert.assertEquals(task.getDescription(), actualTask.getDescription());
        Assert.assertEquals(task.getUser().getId(), actualTask.getUser().getId());
    }

    @Test
    public void testFindByIndexForUserTaskNotFound() {
        Assert.assertNull(taskRepository.findOneByIndex(USER_ID_ONE, NUMBER_OF_ENTRIES / 2));
    }

    @Test
    public void testGetSize() {
        final long expectedNumberOfEntries = taskRepository.getSize() + 1;
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUser(userOne);
        taskRepository.add(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testGetSizeForUser() {
        @NotNull final Task task = new Task();
        task.setName("Test Task");
        task.setUser(userTwo);
        taskRepository.add(task);
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = taskRepository.findAll().get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(invalidId));
        Assert.assertTrue(taskRepository.existsById(validId));
    }

    @Test
    public void testIsNotFoundByIdForUser() {
        @NotNull final String validId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        @NotNull final String invalidId = UUID.randomUUID().toString();
        Assert.assertFalse(taskRepository.existsById(USER_ID_ONE, invalidId));
        Assert.assertTrue(taskRepository.existsById(USER_ID_ONE, validId));
    }

    @Test
    public void testRemove() {
        final long expectedNumberOfEntries = taskRepository.getSize() - 1;
        @NotNull final Task task = taskRepository.findAll().get(0);
        taskRepository.removeOne(task);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final long expectedNumberOfEntries = taskRepository.getSize() - 1;
        @NotNull final String taskId = taskRepository.findAll().get(0).getId();
        taskRepository.removeOneById(taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIdForUser() {
        final long expectedNumberOfEntries = taskRepository.getSize(USER_ID_ONE) - 1;
        @NotNull final String taskId = taskRepository.findAll(USER_ID_ONE).get(0).getId();
        taskRepository.removeOneById(USER_ID_ONE, taskId);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_ONE));
    }

    @Test
    public void testRemoveByIndex() {
        final long expectedNumberOfEntries = taskRepository.getSize() - 1;
        taskRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize());
    }

    @Test
    public void testRemoveByIndexForUser() {
        final long expectedNumberOfEntries = taskRepository.getSize(USER_ID_TWO) - 1;
        taskRepository.removeOneByIndex(USER_ID_TWO, 0);
        Assert.assertEquals(expectedNumberOfEntries, taskRepository.getSize(USER_ID_TWO));
    }

    @Test
    public void testFindAllByProjectIdForUser() {
        @NotNull final List<Task> tasksOne = taskRepository.findAll(USER_ID_ONE);
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByEmptyProjectIdForUser() {
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, "");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectIdForUser() {
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(USER_ID_ONE, projectTwo.getId());
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByProjectId() {
        @NotNull final List<Task> tasksOne = taskRepository.findAll(USER_ID_ONE);
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId(projectOne.getId());
        Assert.assertEquals(tasksOne, tasks);
    }

    @Test
    public void testFindAllByEmptyProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId("");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @Test
    public void testFindAllByIncorrectProjectId() {
        @NotNull final List<Task> tasks = taskRepository.findAllByProjectId("some_id");
        Assert.assertEquals(Collections.emptyList(), tasks);
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

}
