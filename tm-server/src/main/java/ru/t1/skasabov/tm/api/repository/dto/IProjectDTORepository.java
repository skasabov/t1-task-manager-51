package ru.t1.skasabov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

import java.util.List;

public interface IProjectDTORepository extends IUserOwnedDTORepository<ProjectDTO> {

    @NotNull
    List<ProjectDTO> findAllSortByCreated();

    @NotNull
    List<ProjectDTO> findAllSortByStatus();

    @NotNull
    List<ProjectDTO> findAllSortByName();

    @NotNull
    List<ProjectDTO> findAllSortByCreatedForUser(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllSortByStatusForUser(@NotNull String userId);

    @NotNull
    List<ProjectDTO> findAllSortByNameForUser(@NotNull String userId);

}
