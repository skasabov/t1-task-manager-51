package ru.t1.skasabov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.api.service.IConnectionService;
import ru.t1.skasabov.tm.api.service.dto.IProjectDTOService;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.exception.entity.ModelEmptyException;
import ru.t1.skasabov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.skasabov.tm.exception.field.*;
import ru.t1.skasabov.tm.repository.dto.ProjectDTORepository;
import ru.t1.skasabov.tm.repository.dto.TaskDTORepository;

import javax.persistence.EntityManager;
import java.util.*;

public final class ProjectDTOService extends AbstractUserOwnedDTOService<ProjectDTO> implements IProjectDTOService {

    public ProjectDTOService(@NotNull final IConnectionService connectionService) {
        super(connectionService);
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final Collection<ProjectDTO> collection) {
        if (collection == null) return;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects(collection);
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeAll(collection);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<ProjectDTO> addAll(@Nullable final Collection<ProjectDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.addAll(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Collection<ProjectDTO> set(@Nullable final Collection<ProjectDTO> models) {
        if (models == null) return Collections.emptyList();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects();
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.set(models);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return models;
    }

    @NotNull
    @Override
    public ProjectDTO add(@Nullable final ProjectDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.add(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        @NotNull List<ProjectDTO> projects;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            projects = repository.findAll();
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final Sort sortType) {
        if (sortType == null) return findAll();
        @NotNull List<ProjectDTO> projects;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            switch(sortType) {
                case BY_CREATED:
                    projects = repository.findAllSortByCreated();
                    break;
                case BY_STATUS:
                    projects = repository.findAllSortByStatus();
                    break;
                case BY_NAME:
                    projects = repository.findAllSortByName();
                    break;
                default:
                    projects = Collections.emptyList();
                    break;
            }
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            project = repository.findOneById(id);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            project = repository.findOneByIndex(index);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO removeOne(@Nullable final ProjectDTO model) {
        if (model == null) throw new ModelEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(model.getId());
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeOne(model);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = findOneById(id);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(id);
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeOneById(id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneByIndex(@Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize()) throw new IndexIncorrectException();
        @Nullable final ProjectDTO model = findOneByIndex(index);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(model.getId());
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeOneByIndex(index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Override
    @SneakyThrows
    public void removeAll() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects();
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeAll();
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @Override
    @SneakyThrows
    public long getSize() {
        long size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            size = repository.getSize();
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsProject;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            existsProject = repository.existsById(id);
        }
        finally {
            entityManager.close();
        }
        return existsProject;
    }

    @Override
    @SneakyThrows
    public void removeAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjects(userId);
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeAll(userId);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public Boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean existsProject;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            existsProject = repository.existsById(userId, id);
        }
        finally {
            entityManager.close();
        }
        return existsProject;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<ProjectDTO> projects;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            projects = repository.findAll(userId);
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(@Nullable final String userId, @Nullable final Sort sortType) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sortType == null) return findAll(userId);
        @NotNull List<ProjectDTO> projects;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            switch(sortType) {
                case BY_CREATED:
                    projects = repository.findAllSortByCreatedForUser(userId);
                    break;
                case BY_STATUS:
                    projects = repository.findAllSortByStatusForUser(userId);
                    break;
                case BY_NAME:
                    projects = repository.findAllSortByNameForUser(userId);
                    break;
                default:
                    projects = Collections.emptyList();
                    break;
            }
        }
        finally {
            entityManager.close();
        }
        return projects;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            project = repository.findOneById(userId, id);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            project = repository.findOneByIndex(userId, index);
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        long size;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            size = repository.getSize(userId);
        }
        finally {
            entityManager.close();
        }
        return size;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDTO model = findOneById(userId, id);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(id);
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeOneById(userId, id);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @Nullable
    @Override
    @SneakyThrows
    public ProjectDTO removeOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= getSize(userId)) throw new IndexIncorrectException();
        @Nullable final ProjectDTO model = findOneByIndex(userId, index);
        if (model == null) return null;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            @NotNull final ITaskDTORepository taskRepository = new TaskDTORepository(entityManager);
            @NotNull final List<TaskDTO> tasks = taskRepository.findAllByProjectId(model.getId());
            entityManager.getTransaction().begin();
            taskRepository.removeAll(tasks);
            repository.removeOneByIndex(userId, index);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return model;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            project = new ProjectDTO();
            project.setUserId(userId);
            project.setName(name);
            repository.add(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        @NotNull ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            project = new ProjectDTO();
            project.setUserId(userId);
            project.setName(name);
            project.setDescription(description);
            repository.add(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description,
            @Nullable final Date dateBegin,
            @Nullable final Date dateEnd
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) return create(userId, name);
        if (dateBegin == null || dateEnd == null) return create(userId, name, description);
        @NotNull ProjectDTO project;
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            project = new ProjectDTO();
            project.setUserId(userId);
            project.setName(name);
            project.setDescription(description);
            project.setDateBegin(dateBegin);
            project.setDateEnd(dateEnd);
            repository.add(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    public ProjectDTO changeProjectStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index >= getSize(userId)) throw new IndexIncorrectException();
        if (status == null) throw new StatusEmptyException();
        @Nullable final ProjectDTO project = findOneByIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setStatus(status);
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        try {
            @NotNull final IProjectDTORepository repository = new ProjectDTORepository(entityManager);
            entityManager.getTransaction().begin();
            repository.update(project);
            entityManager.getTransaction().commit();
        }
        catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        }
        finally {
            entityManager.close();
        }
        return project;
    }

}
