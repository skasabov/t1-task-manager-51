package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.repository.dto.IDTORepository;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public abstract class AbstractDTORepository<M extends AbstractModelDTO> implements IDTORepository<M> {

    @NotNull
    protected final EntityManager entityManager;

    protected AbstractDTORepository(@NotNull final EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void removeAll(@NotNull final Collection<M> models) {
        for (@NotNull final M model : models) {
            removeOne(model);
        }
    }

    @NotNull
    @Override
    public Collection<M> addAll(@NotNull final Collection<M> models) {
        @NotNull final List<M> result = new ArrayList<>();
        for (@NotNull final M model : models) {
            add(model);
            result.add(model);
        }
        return result;
    }

    @NotNull
    @Override
    public Collection<M> set(@NotNull final Collection<M> models) {
        removeAll();
        return addAll(models);
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void removeOne(@NotNull final M model) {
        entityManager.remove(model);
    }

}
