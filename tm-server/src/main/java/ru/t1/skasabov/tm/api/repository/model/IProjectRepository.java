package ru.t1.skasabov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.model.Project;

import java.util.List;

public interface IProjectRepository extends IUserOwnedRepository<Project> {

    @NotNull
    List<Project> findAllSortByCreated();

    @NotNull
    List<Project> findAllSortByStatus();

    @NotNull
    List<Project> findAllSortByName();

    @NotNull
    List<Project> findAllSortByCreatedForUser(@NotNull String userId);

    @NotNull
    List<Project> findAllSortByStatusForUser(@NotNull String userId);

    @NotNull
    List<Project> findAllSortByNameForUser(@NotNull String userId);

}
