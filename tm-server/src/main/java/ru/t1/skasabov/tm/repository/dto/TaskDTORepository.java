package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.skasabov.tm.dto.model.AbstractModelDTO;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;
import ru.t1.skasabov.tm.dto.model.TaskDTO;
import ru.t1.skasabov.tm.dto.model.UserDTO;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskDTORepository extends AbstractUserOwnedDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t", TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String id) {
        return entityManager.createQuery("SELECT p FROM TaskDTO p WHERE p.id = :id", TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull final Integer index) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t", TaskDTO.class)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDTO t", Long.class)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String id) {
        entityManager.remove(findOneById(id));
    }

    @Override
    public void removeOneByIndex(@NotNull final Integer index) {
        entityManager.remove(findOneByIndex(index));
    }

    @Override
    public void removeAll() {
        @NotNull final List<TaskDTO> tasks = findAll();
        for (@NotNull final TaskDTO task : tasks) {
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.id = :id",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public TaskDTO findOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .setFirstResult(index)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(t) FROM TaskDTO t WHERE t.userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeOneById(@NotNull final String userId, @NotNull final String id) {
        entityManager.remove(findOneById(userId, id));
    }

    @Override
    public void removeOneByIndex(@NotNull final String userId, @NotNull final Integer index) {
        entityManager.remove(findOneByIndex(userId, index));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        @NotNull final List<TaskDTO> tasks = findAll(userId);
        for (@NotNull final TaskDTO task : tasks) {
            entityManager.remove(task);
        }
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByCreated() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t ORDER BY t.created",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByStatus() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t ORDER BY t.status",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByName() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t ORDER BY t.name",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByCreatedForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.created",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByStatusForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.status",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllSortByNameForUser(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId ORDER BY t.name",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.userId = :userId AND t.projectId = :projectId",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjectId(@NotNull final String projectId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t WHERE t.projectId = :projectId",
                        TaskDTO.class)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjects() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t INNER JOIN FETCH ProjectDTO p ON t.projectId = p.id",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjects(@NotNull final String userId) {
        return entityManager.createQuery("SELECT t FROM TaskDTO t INNER JOIN FETCH ProjectDTO p ON t.projectId = p.id WHERE p.userId = :userId",
                        TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByProjects(@NotNull final Collection<ProjectDTO> collection) {
        if (collection.isEmpty()) return Collections.emptyList();
        return entityManager.createQuery("SELECT t FROM TaskDTO t INNER JOIN FETCH ProjectDTO p ON t.projectId = p.id WHERE p.id IN :projects",
                        TaskDTO.class)
                .setParameter("projects", collection.stream().map(AbstractModelDTO::getId).collect(Collectors.toList()))
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUsers() {
        return entityManager.createQuery("SELECT t FROM TaskDTO t INNER JOIN FETCH UserDTO u ON t.userId = u.id",
                        TaskDTO.class)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAllByUsers(@NotNull final Collection<UserDTO> collection) {
        if (collection.isEmpty()) return Collections.emptyList();
        return entityManager.createQuery("SELECT t FROM TaskDTO t INNER JOIN FETCH UserDTO u ON t.userId = u.id WHERE u.id IN :users",
                        TaskDTO.class)
                .setParameter("users", collection.stream().map(AbstractModelDTO::getId).collect(Collectors.toList()))
                .getResultList();
    }
    
}
