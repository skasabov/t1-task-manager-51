package ru.t1.skasabov.tm.api.service;

import liquibase.Liquibase;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull
    EntityManager getEntityManager();

    @NotNull
    EntityManagerFactory factory();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

    @NotNull
    Liquibase getLiquibase();

}
