package ru.t1.skasabov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.skasabov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;

public abstract class AbstractUserOwnedDTORepository<M extends AbstractUserOwnedModelDTO>
        extends AbstractDTORepository<M> implements IUserOwnedDTORepository<M> {

    protected AbstractUserOwnedDTORepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @NotNull
    @Override
    public Boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

}
