package ru.t1.skasabov.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;

public final class JmsLoggerProducer {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final Session session;

    @NotNull
    private final MessageProducer messageProducer;

    @SneakyThrows
    public JmsLoggerProducer() {
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector("tcp://0.0.0.0:61616");
        broker.start();
        @NotNull final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
        System.out.println(text);
    }

    public void send(@NotNull final OperationEvent event) {
        sync(event);
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

}
