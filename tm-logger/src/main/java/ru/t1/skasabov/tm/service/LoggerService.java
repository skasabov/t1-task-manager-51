package ru.t1.skasabov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;

import java.util.LinkedHashMap;
import java.util.Map;

@NoArgsConstructor
public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final String MONGO_URL = System.getProperty("MONGO_URL", "localhost");

    @NotNull
    private final MongoClient mongoClient = new MongoClient(MONGO_URL, 27017);

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("logger");

    @SneakyThrows
    public void log(@NotNull final String json) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
        System.out.println(json);
    }

}
