package ru.t1.skasabov.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectChangeStatusByIndexResponse extends AbstractProjectResponse {

    public ProjectChangeStatusByIndexResponse(@Nullable final ProjectDTO project) {
        super(project);
    }

}
