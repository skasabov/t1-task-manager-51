package ru.t1.skasabov.tm.exception.user;

public final class PasswordHashEmptyException extends AbstractUserException {

    public PasswordHashEmptyException() {
        super("Error! Password hash is empty...");
    }

}
