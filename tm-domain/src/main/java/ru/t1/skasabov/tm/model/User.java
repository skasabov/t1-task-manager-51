package ru.t1.skasabov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "tm_user")
public final class User extends AbstractModel {

    @NotNull
    @Column(nullable = false)
    private String login;

    @NotNull
    @Column(name = "password", nullable = false)
    private String passwordHash;

    @Column
    @Nullable
    private String email = "";

    @Nullable
    @Column(name = "first_name")
    private String firstName = "";

    @Nullable
    @Column(name = "last_name")
    private String lastName = "";

    @Nullable
    @Column(name = "middle_name")
    private String middleName = "";

    @NotNull
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Role role = Role.USUAL;

    @NotNull
    @Column(nullable = false)
    private Boolean locked = false;

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @NotNull
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

}
