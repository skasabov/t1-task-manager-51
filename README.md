# TASK MANAGER

## DEVELOPER INFO

**NAME**: Stas Kasabov

**E-MAIL**: stas@kasabov.ru

**E-MAIL**: stkasabov@yandex.ru

## SOFTWARE

**OS**: Windows 10 Pro 21H2

**JDK**: OPENJDK 1.8.0_322

## HARDWARE

**CPU**: i5-9600K

**RAM**: 16GB

**SSD**: 512GB

## BUILD PROGRAM

```shell
mvn clean install
```

## RUN CLIENT PROGRAM

```shell
java -jar tm-client.jar
```

## TEST CLIENT PROGRAM

```shell
java -jar tm-server.jar
mvn test -P INTEGRATION
```

## BUILD DOCKER CLIENT IMAGE

```shell
docker build -t tm-client tm-client
```

## RUN DOCKER CLIENT IMAGE

```shell
docker run -i --hostname tm-client --name tm-client --network my-net tm-client
```

## RUN SERVER PROGRAM

```shell
java -jar tm-server.jar
```

## TEST SERVER PROGRAM

```shell
mvn test
```

## BUILD DOCKER SERVER IMAGE

```shell
docker build -t tm-server tm-server
```

## RUN DOCKER SERVER IMAGE

```shell
docker network create my-net
docker run -v /data/volume/postgres:/var/lib/postgresql/data -e POSTGRES_PASSWORD=postgres -e POSTGRES_DB=taskmanager -d -p 5432:5432 --hostname postgres --name postgres --network my-net postgres
docker run -d -p 8080:8080 -p 61616:61616 --hostname tm-server --name tm-server --network my-net tm-server
```

## RUN LOGGER PROGRAM

```shell
java -jar tm-logger.jar
```

## TEST LOGGER PROGRAM

```shell
mvn test
```

## BUILD DOCKER LOGGER IMAGE

```shell
docker build -t tm-logger tm-logger
```

## RUN DOCKER LOGGER IMAGE

```shell
docker run --name mongo --network my-net --hostname mongo -d -p 27017:27017 mongo:latest
docker run -d --name tm-logger --hostname tm-logger --network my-net tm-logger
```
