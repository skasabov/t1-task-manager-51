package ru.t1.skasabov.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.dto.request.ProjectUpdateByIndexRequest;
import ru.t1.skasabov.tm.util.TerminalUtil;

@NoArgsConstructor
public final class ProjectUpdateByIndexCommand extends AbstractProjectCommand {

    @NotNull
    private static final String NAME = "project-update-by-index";

    @NotNull
    private static final String DESCRIPTION = "Update project by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[UPDATE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectUpdateByIndexRequest request = new ProjectUpdateByIndexRequest(index, name, description);
        request.setToken(getToken());
        getProjectEndpoint().updateProjectByIndex(request);
    }

}
