package ru.t1.skasabov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.skasabov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.skasabov.tm.dto.request.*;
import ru.t1.skasabov.tm.dto.response.*;

@NoArgsConstructor
public final class DomainEndpoint implements IDomainEndpoint {

    @NotNull
    @Override
    public DataBackupLoadResponse loadDataBackup(@NotNull final DataBackupLoadRequest request) {
        return IDomainEndpoint.newInstance().loadDataBackup(request);
    }

    @NotNull
    @Override
    public DataBackupSaveResponse saveDataBackup(@NotNull final DataBackupSaveRequest request) {
        return IDomainEndpoint.newInstance().saveDataBackup(request);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadDataBase64(@NotNull final DataBase64LoadRequest request) {
        return IDomainEndpoint.newInstance().loadDataBase64(request);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveDataBase64(@NotNull final DataBase64SaveRequest request) {
        return IDomainEndpoint.newInstance().saveDataBase64(request);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadDataBinary(@NotNull final DataBinaryLoadRequest request) {
        return IDomainEndpoint.newInstance().loadDataBinary(request);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveDataBinary(@NotNull final DataBinarySaveRequest request) {
        return IDomainEndpoint.newInstance().saveDataBinary(request);
    }

    @NotNull
    @Override
    public DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(@NotNull final DataJsonLoadFasterXmlRequest request) {
        return IDomainEndpoint.newInstance().loadDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(@NotNull final DataJsonSaveFasterXmlRequest request) {
        return IDomainEndpoint.newInstance().saveDataJsonFasterXml(request);
    }

    @NotNull
    @Override
    public DataJsonLoadJaxBResponse loadDataJsonJaxB(@NotNull final DataJsonLoadJaxBRequest request) {
        return IDomainEndpoint.newInstance().loadDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public DataJsonSaveJaxBResponse saveDataJsonJaxB(@NotNull final DataJsonSaveJaxBRequest request) {
        return IDomainEndpoint.newInstance().saveDataJsonJaxB(request);
    }

    @NotNull
    @Override
    public DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(@NotNull final DataXmlLoadFasterXmlRequest request) {
        return IDomainEndpoint.newInstance().loadDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(@NotNull final DataXmlSaveFasterXmlRequest request) {
        return IDomainEndpoint.newInstance().saveDataXmlFasterXml(request);
    }

    @NotNull
    @Override
    public DataXmlLoadJaxBResponse loadDataXmlJaxB(@NotNull final DataXmlLoadJaxBRequest request) {
        return IDomainEndpoint.newInstance().loadDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public DataXmlSaveJaxBResponse saveDataXmlJaxB(@NotNull final DataXmlSaveJaxBRequest request) {
        return IDomainEndpoint.newInstance().saveDataXmlJaxB(request);
    }

    @NotNull
    @Override
    public DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(@NotNull final DataYamlLoadFasterXmlRequest request) {
        return IDomainEndpoint.newInstance().loadDataYamlFasterXml(request);
    }

    @NotNull
    @Override
    public DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(@NotNull final DataYamlSaveFasterXmlRequest request) {
        return IDomainEndpoint.newInstance().saveDataYamlFasterXml(request);
    }

}
